#include <stdio.h>
#include "../../include/local.h"

#define ADD_OUT_OF_RANGE( _result )                 ( _result = _result | DIAGRAM_RESULT_OUT_OF_RANGE )
#define ADD_SHORTAGE_HYS_SIZE( _result )            ( _result = _result | DIAGRAM_RESULT_SHORTAGE_OF_HYS_SIZE )

#define IS_DIAGRAM_OUT_OF_RANGE( _result )          ( ( _result & DIAGRAM_RESULT_OUT_OF_RANGE )         == DIAGRAM_RESULT_OUT_OF_RANGE )
#define IS_DIAGRAM_SHORTAGE_OF_HYS_SIZE( _result )  ( ( _result & DIAGRAM_RESULT_SHORTAGE_OF_HYS_SIZE ) == DIAGRAM_RESULT_SHORTAGE_OF_HYS_SIZE )

#define IS_DIAGRAM_SUCCESS( _result )               ( !(IS_DIAGRAM_ERROR( _result )) )
#define IS_DIAGRAM_ERROR( _result )                 (   IS_DIAGRAM_OUT_OF_RANGE( _result )              \
                                                     || IS_DIAGRAM_SHORTAGE_OF_HYS_SIZE( _result ) )

typedef enum {

	BIT0 = 0b00000001,
	BIT1 = 0b00000010,
	BIT2 = 0b00000100,
	BIT3 = 0b00001000,
	BIT4 = 0b00010000,
	BIT5 = 0b00100000,
	BIT6 = 0b01000000,
	BIT7 = 0b10000000,

} BIT_T;

typedef enum {

    DIAGRAM_RESULT_OUT_OF_RANGE         = BIT0,
    DIAGRAM_RESULT_SHORTAGE_OF_HYS_SIZE = BIT1,

} DIAGRAM_RESULT_T;


PRIVATE DIAGRAM_RESULT_T calcDiagram( unsigned int num )
{
    DIAGRAM_RESULT_T Result;

    switch ( num ) {
        case 1:     ADD_OUT_OF_RANGE( Result );         break;
        case 2:     ADD_SHORTAGE_HYS_SIZE( Result );    break;
        default:                                        break;
    }

    return Result;
}

PUBLIC void main()
{
    unsigned int        num;
    DIAGRAM_RESULT_T    Result;

    scanf( "%d", &num );
    Result = calcDiagram( num );

    if ( IS_DIAGRAM_SUCCESS( Result ) )
    {
        printf( "Diagram is SUCCESS !!\n" );
    }

    if ( IS_DIAGRAM_OUT_OF_RANGE( Result ) )
    {
        printf( "Diagram is out of range !!\n" );
    }

    if ( IS_DIAGRAM_SHORTAGE_OF_HYS_SIZE( Result ) )
    {
        printf( "Diagram is shortage hys size !!\n" );
    }

    if ( IS_DIAGRAM_ERROR( Result ) )
    {
        printf( "Diagram is ERROR !!\n" );
    }

}
